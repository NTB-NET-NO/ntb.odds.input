// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Match.cs" company="NTB">
//   NTB
// </copyright>
// <summary>
//   The matches.
// </summary>
// --------------------------------------------------------------------------------------------------------------------


using System;
using System.Collections.Generic;
using NTB.Odds.Input.Domain.Enums;

namespace NTB.Odds.Input.Domain.Classes
{
    /// <summary>
    /// The matches.
    /// </summary>
    public class Match
    {
        /// <summary>
        /// Gets or sets the match id.
        /// </summary>
        public int MatchId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public int SportId { get; set; }

        /// <summary>
        /// Gets or sets the sport id.
        /// </summary>
        public string SportName { get; set; }

        /// <summary>
        /// Gets or sets the home team.
        /// </summary>
        public string HomeTeam { get; set; }

        /// <summary>
        /// Gets or sets the away team.
        /// </summary>
        public string AwayTeam { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime MatchDate { get; set; }

        /// <summary>
        /// Gets or sets the tournament id.
        /// </summary>
        public int TournamentId { get; set; }

        /// <summary>
        ///     Gets or sets the Tournament name
        /// </summary>
        public string TournamentName { get; set; }

        /// <summary>
        ///     Gets or sets the Away Team id
        /// </summary>
        public int AwayTeamId { get; set; }

        /// <summary>
        ///     Gets or sets the Home Team id
        /// </summary>
        public int HomeTeamId { get; set; }
        
        /// <summary>
        ///     Gets or sets the Match Start Time
        /// </summary>
        public int MatchStartTime { get; set; }

        /// <summary>
        ///     Gets or sets the Match comment Text
        /// </summary>
        public string MatchComment { get; set; }

        /// <summary>
        ///     Gets or sets the bet object
        /// </summary>
        public BetObject BetObject { get; set; }

        /// <summary>
        ///     Gets or sets if this match is viewed on TV
        /// </summary>
        public bool ViewedOnTv { get; set; }

        /// <summary>
        ///     Gets or sets Match Type for this match
        /// </summary>
        public MatchType MatchType { get; set; }

        /// <summary>
        ///     Gets or sets the Tv Channel this match is aired
        /// </summary>
        public Broadcaster Broadcaster { get; set; }

        public string SelectedOdds { get; set; }

        public string SelectedMatchType { get; set; }
    }

    public class TestMatch
    {
        public TestMatch()
        {
            var foo = new Match();

            var bar = foo.MatchType.CompareTo(MatchType.HighMatch);
        }
    }
}