﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Odds.Input.Domain.Classes
{
    public class ArticleViewModel
    {
        public List<Match> Matches { get; set; }
        public List<Broadcaster> Broadcasters { get; set; }
    }
}