﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Odds.Input.Domain.Classes
{
    public class OddsDocument
    {
        public string Id { get; set; }

        public string Title { get; set; }
    }
}
