﻿using System;
using System.Collections.Generic;

namespace NTB.Odds.Input.Domain.Classes
{
    public class UserProfile
    {
        public Guid UserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }

        public string UserName { get; set; }

        public string Email { get; set; }

        public bool IsOnline { get; set; }

        public DateTime LastLoginDate { get; set; }

        public string PassWord { get; set; }

        public string Role { get; set; }

        public WorkRole WorkRole { get; set; }

        public string[] Roles { get; set; }

        /// <summary>
        ///     Gets or sets the signature string
        /// </summary>
        public string Signature { get; set; }
    }
}
