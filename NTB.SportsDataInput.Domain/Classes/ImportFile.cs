﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Odds.Input.Domain.Classes
{
    public class ImportFile
    {
        public string FullName { get; set; }

        public string FileName { get; set; }

        public DateTime CreationDate { get; set; }

        public long FileSize { get; set; }

        public string DirectoryName { get; set; }
    }
}
