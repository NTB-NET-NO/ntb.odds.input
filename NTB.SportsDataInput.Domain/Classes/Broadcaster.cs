﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Odds.Input.Domain.Classes
{
    public class Broadcaster
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
