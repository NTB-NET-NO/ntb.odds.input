﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Odds.Input.Domain.Classes
{
    public class BettingObject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public List<BettingSelection> Selections { get; set; }
    }
}
