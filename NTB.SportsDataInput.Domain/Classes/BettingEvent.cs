﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Odds.Input.Domain.Classes
{
    public class BettingEvent
    {
        /// <summary>
        ///     Gets or sets the event id for this event id
        /// </summary>
        public int EventId { get; set; }

        /// <summary>
        ///     Gets or sets the home team object for this event id
        /// </summary>
        public Team HomeTeam { get; set; }

        /// <summary>
        ///     Gets or sets the away team object for this event id
        /// </summary>
        public Team AwayTeam { get; set; }

        /// <summary>
        ///     Gets or sets the match start date and time for this event id
        /// </summary>
        public DateTime MatchStartDate { get; set; }

        /// <summary>
        ///     Gets or sets the Tournament object for this event id
        /// </summary>
        public Tournament Tournament { get; set; }

        /// <summary>
        ///     Gets or sets the betting object text for this event id
        /// </summary>
        public BettingObject BettingObject { get; set; }

        /// <summary>
        ///     Gets or sets the description text for this event id
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Gets or sets the match start time
        /// </summary>
        public int MatchStartTime { get; set; }

        /// <summary>
        ///     Gets or sets the broadcaster
        /// </summary>
        public Broadcaster Broadcaster { get; set; }

        /// <summary>
        ///     Gets or sets the selected match type
        /// </summary>
        public string SelectedMatchType { get; set; }

        /// <summary>
        ///     Gets or sets the selected odds
        /// </summary>
        public string SelectedOdds { get; set; }

        /// <summary>
        ///     Gets or sets the user profile
        /// </summary>
        public UserProfile UserProfile { get; set; }
    }
}
