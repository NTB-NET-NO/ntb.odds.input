﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Odds.Input.Domain.Classes
{
    public class BetObject
    {
        public int Id { get; set; }

        public int Number { get; set; }

        public DateTime BetDate { get; set; }

        public string HomeSelectionOdds { get; set; }

        public string DrawSelectionOdds { get; set; }

        public string AwaySelectionOdds { get; set; }
    }
}
