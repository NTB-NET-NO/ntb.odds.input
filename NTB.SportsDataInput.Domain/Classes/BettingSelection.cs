﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Odds.Input.Domain.Classes
{
    public class BettingSelection
    {
        public int Id { get; set; }

        public string Value { get; set; }

        public bool WithDrawn { get; set; }

        public string Name { get; set; }

        public string ShortName { get; set; }
    }
}
