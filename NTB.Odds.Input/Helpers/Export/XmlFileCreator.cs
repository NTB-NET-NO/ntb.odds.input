using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml;
using NTB.Odds.Input.DataMappers.Documents;
using NTB.Odds.Input.Domain.Classes;

namespace NTB.Odds.Input.Helpers.Export
{
    public class XmlFileCreator
    {
        // This model is used to create the Xml structure

        private XmlDocument _doc;

        public XmlElement XmlRoot;

        public string DocumentTitle { get; set; }

        public List<Sport> Sports { get; set; }

        public List<BettingEvent> BettingEvents { get; set; }

        public UserProfile UserProfile { get; set; }

        private string _Filename { get; set; }

        private Document _Document { get; set; }

        public XmlFileCreator()
        {
            _doc = new XmlDocument();

        }

        public XmlElement CreateRootElement()
        {
            return _doc.CreateElement("betting-content");
        }
        public void CreateXml()
        {
            XmlRoot = CreateRootElement();

            string fullfilename = CreateFullFileName();
            var document = new Document
            {
                Name = _Filename,
                CreationDate = DateTime.Now,
                CreationTime = Convert.ToInt32(DateTime.Now.ToShortTimeString().Replace(":", "").Substring(0, 4)),
                Version = 1
            };

            var documentDataMapper = new DocumentDataMapper();
            var documents = documentDataMapper.GetDocumentsByDate(DateTime.Now);

            // Checking which version this really is
            if (documents.Count > 0)
            {
                document.Version = documents.Count + 1;
            }

            documentDataMapper.InsertOne(document);

            _Document = document;
            XmlRoot.AppendChild(CreateHeaderElement());

            XmlRoot.AppendChild(CreateRootMetaDataElement());

            XmlElement xmlEvent = CreateBettingEventElements();
            // XmlRoot.AppendChild(xmlEvent);

            _doc.AppendChild(XmlRoot);

            _doc.Save(fullfilename);

        }

        private void CreateDirectory(string outpath)
        {
            // Create the Directory Info object used to check if directory exists or not
            DirectoryInfo di = new DirectoryInfo(outpath);

            // Checking if directory exists
            if (!di.Exists)
            {
                // It does not exist, so we create it
                di.Create();
            }
        }

        public string CreateFileName()
        {
            // Get todays date and put it into a variable (less typing)
            var today = DateTime.Now;

            // Create filename - remember padLeft to have at least two character length
            string fileName = today.Year
                              + "-" + today.Month.ToString().PadLeft(2, '0')
                              + "-" + today.Day.ToString().PadLeft(2, '0')
                              + "T"
                              + today.Hour.ToString().PadLeft(2, '0')
                              + today.Minute.ToString().PadLeft(2, '0')
                              + today.Second.ToString().PadLeft(2, '0')
                              + "_NTB_BML.xml";

            return fileName;
        }

        

        public string CreateFullFileName()
        {
            // Getting the output path
            var outpath = ConfigurationManager.AppSettings["OutputFolder"];

            // Checking if the directory exists
            CreateDirectory(outpath);

            _Filename = CreateFileName();

            // Return full filename with output path
            return outpath + @"\" + _Filename;
        }

        public XmlElement CreateCreatorElement()
        {
            XmlElement xmlCreatorElement = _doc.CreateElement("creator");
            xmlCreatorElement.SetAttribute("firstname", UserProfile.FirstName);
            xmlCreatorElement.SetAttribute("lastname", UserProfile.LastName);
            xmlCreatorElement.SetAttribute("signature", UserProfile.Signature);

            return xmlCreatorElement;
        }

        public XmlElement CreateRootMetaDataElement()
        {
            XmlElement xmlMetaData = _doc.CreateElement("betting-metadata");
            XmlElement xmlBettingTitle = _doc.CreateElement("betting-title");
            XmlText xmlText = _doc.CreateTextNode(DocumentTitle);
            xmlBettingTitle.AppendChild(xmlText);
            xmlMetaData.AppendChild(xmlBettingTitle);
            foreach (Sport sport in Sports)
            {
                XmlElement xmlContentCode = _doc.CreateElement("betting-content-code");
                xmlContentCode.SetAttribute("code-type", "subjectcode");
                xmlContentCode.SetAttribute("code-key", sport.IptcCode.ToString());
                xmlContentCode.SetAttribute("code-name", sport.Name);
                
                xmlMetaData.AppendChild(xmlContentCode);

            }

            return xmlMetaData;
        }

        private string CreateDateTimeFromString(DateTime matchDate, string matchTime)
        {
            if (matchTime.Length < 4)
            {
                matchTime = matchTime.PadLeft(4, '0');
            }
            string xmlDateTime = matchDate.Year + "-"
                                 + matchDate.Month.ToString().PadLeft(2,'0') + "-"
                                 + matchDate.Day.ToString().PadLeft(2, '0')
                                 + "T"
                                 + matchTime.Substring(0, 2) + ":"
                                 + matchTime.Substring(2, 2) + ":00";

            return xmlDateTime;
        }

        public XmlElement CreateBettingEventElements()
        {
            XmlElement xmlBettingEventElement = null;
            foreach (BettingEvent bettingEvent in BettingEvents)
            {
                xmlBettingEventElement = _doc.CreateElement("betting-event");
                xmlBettingEventElement.SetAttribute("id", bettingEvent.EventId.ToString());

                XmlElement xmlEventMetaDataElement = _doc.CreateElement("event-metadata");
                xmlEventMetaDataElement.SetAttribute("date-coverage-type", "event");
                xmlEventMetaDataElement.SetAttribute("event-key", "E" + bettingEvent.EventId);
                xmlEventMetaDataElement.SetAttribute("event-status", "pre-event");
                xmlEventMetaDataElement.SetAttribute("event-status-note", "pre-event");
                xmlEventMetaDataElement.SetAttribute("start-date-time", CreateDateTimeFromString(bettingEvent.MatchStartDate, bettingEvent.MatchStartTime.ToString()));
                xmlEventMetaDataElement.SetAttribute("event-name", bettingEvent.HomeTeam.TeamName + " - " + bettingEvent.AwayTeam.TeamName);

                XmlElement xmlBettingPropertyElement = _doc.CreateElement("betting-property");
                xmlBettingPropertyElement.SetAttribute("formal-name", "tournament-name");
                xmlBettingPropertyElement.SetAttribute("value", bettingEvent.Tournament.TournamentName);
                xmlBettingEventElement.AppendChild(xmlBettingPropertyElement);

                xmlBettingPropertyElement = CreateBettingPropertyElement("tournament-id", bettingEvent.Tournament.TournamentId.ToString());
                xmlBettingEventElement.AppendChild(xmlBettingPropertyElement);
                xmlBettingPropertyElement = CreateBettingPropertyElement("broadcaster", bettingEvent.Broadcaster.Name);
                xmlBettingEventElement.AppendChild(xmlBettingPropertyElement);

                // Add properties in bettingEvent so we get which value has been selected
                xmlBettingPropertyElement = CreateBettingPropertyElement("SelectedMatchType", bettingEvent.SelectedMatchType);
                xmlBettingEventElement.AppendChild(xmlBettingPropertyElement);
                // Add properties so we also get high odds, todays game and so on
                xmlBettingPropertyElement = CreateBettingPropertyElement("SelectedOdds", bettingEvent.SelectedOdds);
                xmlBettingEventElement.AppendChild(xmlBettingPropertyElement);
                


                xmlBettingEventElement.AppendChild(xmlEventMetaDataElement);

                xmlBettingEventElement.AppendChild(CreateTeamElement(bettingEvent, "home"));
                xmlBettingEventElement.AppendChild(CreateTeamElement(bettingEvent, "away"));

                xmlBettingEventElement.AppendChild(CreateEventDescriptionElement(bettingEvent));
                XmlRoot.AppendChild(xmlBettingEventElement);
            }

            return xmlBettingEventElement;
        }

        public XmlElement CreateBettingPropertyElement(string formalName, string value)
        {
            XmlElement xmlBettingPropertyElement = _doc.CreateElement("betting-property");
            xmlBettingPropertyElement.SetAttribute("formal-name", formalName);
            xmlBettingPropertyElement.SetAttribute("value", value);

            return xmlBettingPropertyElement;
        }

        public XmlElement CreateHeaderElement()
        {
            XmlElement xmlHeaderElement = _doc.CreateElement("header");
            XmlElement xmlSentElement = _doc.CreateElement("sent");

            DateTime today = DateTime.Now;
            string dateTime = today.Year + "-"
                              + today.Month.ToString().PadLeft(2, '0') + "-"
                              + today.Day.ToString().PadLeft(2, '0')
                              + "T"
                              + today.Hour.ToString().PadLeft(2, '0') + ":"
                              + today.Minute.ToString().PadLeft(2, '0') + ":"
                              + today.Second.ToString().PadLeft(2, '0');
            XmlText xmlText = _doc.CreateTextNode(dateTime);
            xmlSentElement.AppendChild(xmlText);

            xmlHeaderElement.AppendChild(xmlSentElement);

            XmlElement xmlFileElement = _doc.CreateElement("file");
            string fileName = CreateFileName();
            xmlText = _doc.CreateTextNode(fileName);
            xmlFileElement.AppendChild(xmlText);
            xmlHeaderElement.AppendChild(xmlFileElement);
            xmlHeaderElement.AppendChild(CreateCreatorElement());

            XmlElement xmlVersionElement = _doc.CreateElement("version");
            xmlText = _doc.CreateTextNode(_Document.Version.ToString());
            xmlVersionElement.AppendChild(xmlText);

            xmlHeaderElement.AppendChild(xmlVersionElement);
            return xmlHeaderElement;
        }

        public XmlElement CreateTeamElement(BettingEvent bettingEvent, string alignment)
        {
            XmlElement xmlTeamElement = _doc.CreateElement("team");
            

            XmlElement xmlTeamMetaDataElement = _doc.CreateElement("team-metadata");
            XmlElement xmlTeamMetaDataNameElement = _doc.CreateElement("name");
            if (alignment == "home")
            {
                xmlTeamElement.SetAttribute("id", bettingEvent.HomeTeam.TeamId.ToString());
                xmlTeamMetaDataElement.SetAttribute("team-key", "T" + bettingEvent.HomeTeam.TeamId);
                xmlTeamMetaDataElement.SetAttribute("alignment", alignment);

                xmlTeamMetaDataNameElement.SetAttribute("first", bettingEvent.HomeTeam.TeamName);
                xmlTeamMetaDataNameElement.SetAttribute("full", bettingEvent.HomeTeam.TeamName);
                xmlTeamMetaDataNameElement.SetAttribute("abbreviation", bettingEvent.HomeTeam.TeamName);
                xmlTeamMetaDataNameElement.SetAttribute("nickname", bettingEvent.HomeTeam.TeamName);

            }
            else if (alignment == "away")
            {
                xmlTeamElement.SetAttribute("id", bettingEvent.AwayTeam.TeamId.ToString());
                xmlTeamMetaDataElement.SetAttribute("team-key", "T" + bettingEvent.AwayTeam.TeamId);
                xmlTeamMetaDataElement.SetAttribute("alignment", alignment);

                xmlTeamMetaDataNameElement.SetAttribute("first", bettingEvent.AwayTeam.TeamName);
                xmlTeamMetaDataNameElement.SetAttribute("full", bettingEvent.AwayTeam.TeamName);
                xmlTeamMetaDataNameElement.SetAttribute("abbreviation", bettingEvent.AwayTeam.TeamName);
                xmlTeamMetaDataNameElement.SetAttribute("nickname", bettingEvent.AwayTeam.TeamName);

            }

            xmlTeamMetaDataElement.AppendChild(xmlTeamMetaDataNameElement);
            xmlTeamElement.AppendChild(xmlTeamMetaDataElement);
            xmlTeamElement.AppendChild(CreateBettingObject(alignment, bettingEvent));

            return xmlTeamElement;
        }

        public XmlElement CreateBettingObject (string alignment, BettingEvent bettingEvent)
        {
            XmlElement xmlBettingObjectElement = _doc.CreateElement("betting-object");
            XmlElement xmlBettingObjectMetaDataElement = _doc.CreateElement("betting-object-metadata");
            // TODO: Create code so we can support more than just this type of game
            xmlBettingObjectMetaDataElement.SetAttribute("name", "HUB-fulltid"); 
            
            xmlBettingObjectMetaDataElement.SetAttribute("key", "B" + bettingEvent.EventId);
            xmlBettingObjectMetaDataElement.SetAttribute("betting-number", bettingEvent.BettingObject.Id.ToString());

            xmlBettingObjectElement.AppendChild(xmlBettingObjectMetaDataElement);


            XmlElement xmlBettingSelection = _doc.CreateElement("betting-selection");
            if (alignment == "home")
            {
                XmlElement xmlBettingSelectionMetaDataElement = _doc.CreateElement("betting-selection-metadata");

                // TODO: Create code so we can support more than just this type of game
                xmlBettingSelectionMetaDataElement.SetAttribute("type", "odds");
                xmlBettingSelectionMetaDataElement.SetAttribute("sequence-number", "1");
                xmlBettingSelectionMetaDataElement.SetAttribute("value", bettingEvent.BettingObject.Selections.Single(x => x.Name=="home").Value);
                xmlBettingSelectionMetaDataElement.SetAttribute("withdrawn", "false");

                XmlElement xmlBettingSelectionMetaDataNameElement = _doc.CreateElement("name");
                xmlBettingSelectionMetaDataNameElement.SetAttribute("fullname", "home");
                xmlBettingSelectionMetaDataNameElement.SetAttribute("abbreviation", "H");
                xmlBettingSelectionMetaDataElement.AppendChild(xmlBettingSelectionMetaDataNameElement);
                xmlBettingSelection.AppendChild(xmlBettingSelectionMetaDataElement);

                // Support for Draw:
                xmlBettingSelectionMetaDataElement = _doc.CreateElement("betting-selection-metadata");

                // TODO: Create code so we can support more than just this type of game
                xmlBettingSelectionMetaDataElement.SetAttribute("type", "odds");
                xmlBettingSelectionMetaDataElement.SetAttribute("sequence-number", "2");
                xmlBettingSelectionMetaDataElement.SetAttribute("value", bettingEvent.BettingObject.Selections.Single(x => x.Name == "draw").Value);
                xmlBettingSelectionMetaDataElement.SetAttribute("withdrawn", "false");

                xmlBettingSelectionMetaDataNameElement = _doc.CreateElement("name");
                xmlBettingSelectionMetaDataNameElement.SetAttribute("fullname", "draw");
                xmlBettingSelectionMetaDataNameElement.SetAttribute("abbreviation", "U");
                xmlBettingSelectionMetaDataElement.AppendChild(xmlBettingSelectionMetaDataNameElement);

                xmlBettingSelection.AppendChild(xmlBettingSelectionMetaDataElement);

            } else if (alignment == "away")
            {
                XmlElement xmlBettingSelectionMetaDataElement = _doc.CreateElement("betting-selection-metadata");

                // TODO: Create code so we can support more than just this type of game
                xmlBettingSelectionMetaDataElement.SetAttribute("type", "odds");
                xmlBettingSelectionMetaDataElement.SetAttribute("sequence-number", "3");
                xmlBettingSelectionMetaDataElement.SetAttribute("value", bettingEvent.BettingObject.Selections.Single(x => x.Name == "away").Value);
                xmlBettingSelectionMetaDataElement.SetAttribute("withdrawn", "false");

                XmlElement xmlBettingSelectionMetaDataNameElement = _doc.CreateElement("name");
                xmlBettingSelectionMetaDataNameElement.SetAttribute("fullname", "away");
                xmlBettingSelectionMetaDataNameElement.SetAttribute("abbreviation", "B");
                xmlBettingSelectionMetaDataElement.AppendChild(xmlBettingSelectionMetaDataNameElement);

                // Append value
                xmlBettingSelection.AppendChild(xmlBettingSelectionMetaDataElement);


                // Support for Draw:
                xmlBettingSelectionMetaDataElement = _doc.CreateElement("betting-selection-metadata");

                // TODO: Create code so we can support more than just this type of game
                xmlBettingSelectionMetaDataElement.SetAttribute("type", "odds");
                xmlBettingSelectionMetaDataElement.SetAttribute("sequence-number", "2");
                xmlBettingSelectionMetaDataElement.SetAttribute("value", bettingEvent.BettingObject.Selections.Single(x => x.Name == "draw").Value);
                xmlBettingSelectionMetaDataElement.SetAttribute("withdrawn", "false");

                xmlBettingSelectionMetaDataNameElement = _doc.CreateElement("name");
                xmlBettingSelectionMetaDataNameElement.SetAttribute("fullname", "draw");
                xmlBettingSelectionMetaDataNameElement.SetAttribute("abbreviation", "U");
                xmlBettingSelectionMetaDataElement.AppendChild(xmlBettingSelectionMetaDataNameElement);

                xmlBettingSelection.AppendChild(xmlBettingSelectionMetaDataElement);xmlBettingSelection.AppendChild(xmlBettingSelectionMetaDataElement);
            }
            
            xmlBettingObjectElement.AppendChild(xmlBettingObjectMetaDataElement);
            xmlBettingObjectElement.AppendChild(xmlBettingSelection);

            return xmlBettingObjectElement;
        }

        public XmlElement CreateEventDescriptionElement(BettingEvent bettingEvent)
        {
            XmlElement xmlDescriptionElement = _doc.CreateElement("description");
            XmlElement xmlDescriptionTextElement = _doc.CreateElement("text");

            XmlText xmlText = _doc.CreateTextNode(bettingEvent.Description.Replace("\r\n", "<br />"));
            xmlDescriptionTextElement.AppendChild(xmlText);
            xmlDescriptionElement.AppendChild(xmlDescriptionTextElement);

            return xmlDescriptionElement;
        }
    }

}