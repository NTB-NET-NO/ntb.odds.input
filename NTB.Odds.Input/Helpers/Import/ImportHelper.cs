﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using NTB.Odds.Input.Domain.Classes;

namespace NTB.Odds.Input.Helpers.Import
{
    public class ImportHelper
    {
        private XmlDocument XmlDoc { get; set; }

        private XmlNamespaceManager _namespaceManager;

        public ImportHelper(XmlDocument xmlDocument)
        {
            XmlDoc = xmlDocument;

            SetNameSpace(xmlDocument);
        }
        private void SetNameSpace(XmlDocument document)
        {
            string xmlNameSpace = ConfigurationManager.AppSettings["XmlNameSpace"];
            _namespaceManager  = new XmlNamespaceManager(document.NameTable);
            _namespaceManager.AddNamespace("ns1", xmlNameSpace);
        }


        public string GetDocumentTitle()
        {
            const string xpath = "/ns1:ResultsResponse/ns1:Langoddsen/@Name";

            var selectSingleNode = XmlDoc.SelectSingleNode(xpath, _namespaceManager);
            return selectSingleNode != null ? selectSingleNode.Value : string.Empty;
        }

        public string GetWeekNumber()
        {
            const string xpath = "/ns1:ResultsResponse/ns1:Langoddsen/@Id";

            var selectSingleNode = XmlDoc.SelectSingleNode(xpath, _namespaceManager);
            return selectSingleNode != null ? selectSingleNode.Value : string.Empty;
        }

        public List<Match> GetMatches()
        {
            const string xpath = "/ns1:ResultsResponse/ns1:Langoddsen/ns1:Event[ns1:BetObject/@Name='HUB-fulltid']";

            var nodes = XmlDoc.SelectNodes(xpath, _namespaceManager);

            List<Match> matches = new List<Match>();
            if (nodes == null)
            {
                return matches;
            }

            // Looping over nodes
            foreach (XmlNode node in nodes)
            {
                var match = new Match();

                var hometeam = GetHomeTeam(node);
                var awayteam = GetAwayTeam(node);

                var sport = GetSport(node);
                match.MatchId = GetMatchId(node);
                match.HomeTeam = hometeam.TeamName;
                match.HomeTeamId = hometeam.TeamId;
                match.AwayTeam = awayteam.TeamName;
                match.AwayTeamId = awayteam.TeamId;
                match.MatchDate = GetMatchDate(node);
                match.MatchStartTime = Convert.ToInt32(match.MatchDate.ToShortTimeString().Replace(":", ""));

                match.SportId = sport.Id;
                match.SportName = sport.Name;

                var tournament = GetTournament(node);
                match.TournamentId = tournament.TournamentId;
                match.TournamentName = tournament.TournamentName;

                match.BetObject = GetBetobjectValues(node);

                matches.Add(match);
            }

            return matches;
        }

        private DateTime GetMatchDate(XmlNode node)
        {
            DateTime matchDate = DateTime.Today;
            // Get the sportId
            var nodeMatchDate = node.SelectSingleNode("ns1:Time", _namespaceManager);
            if (nodeMatchDate != null)
            {
                matchDate = Convert.ToDateTime(nodeMatchDate.InnerText);
            }

            return matchDate;
        }

        private int GetMatchId(XmlNode node)
        {
            int id = 0;
            // Get the sportId
            var nodeMatchId = node.SelectSingleNode("ns1:MatchId", _namespaceManager);
            if (nodeMatchId != null)
            {
                id = Convert.ToInt32(nodeMatchId.InnerText);
            }

            return id;
        }

        private Team GetHomeTeam(XmlNode node)
        {
            var team = new Team();
            var nodeTeam = node.SelectSingleNode("ns1:Item[1]", _namespaceManager);
            if (nodeTeam != null)
            {
                if (nodeTeam.Attributes != null)
                {
                    team.TeamId = Convert.ToInt32(nodeTeam.Attributes["id"].Value);
                }

                var xmlElement = nodeTeam["ns1:Name"];
                if (xmlElement != null) team.TeamName = xmlElement.InnerText;
            }
            return team;
        }

        private Team GetAwayTeam(XmlNode node)
        {
            var team = new Team();
            var nodeTeam = node.SelectSingleNode("ns1:Item[2]", _namespaceManager);
            if (nodeTeam != null)
            {
                if (nodeTeam.Attributes != null)
                {

                    team.TeamId = Convert.ToInt32(nodeTeam.Attributes["id"].Value);
                }
                var xmlElement = nodeTeam["ns1:Name"];
                if (xmlElement != null) team.TeamName = xmlElement.InnerText;
            }
            return team;
        }

        private Sport GetSport(XmlNode node)
        {
            var sport = new Sport();
            
            // Get the sportId
            var nodeSportId = node.SelectSingleNode("ns1:SportId", _namespaceManager);
            if (nodeSportId != null)
            {
                sport.Id = Convert.ToInt32(nodeSportId.InnerText);
            }

            var nodeSportName = node.SelectSingleNode("ns1:SportName", _namespaceManager);
            if (nodeSportName != null)
            {
                sport.Name = nodeSportName.InnerText;
            }

            return sport;
        }

        private Tournament GetTournament(XmlNode node)
        {
            var tournament = new Tournament();

            // Get the sportId
            var nodeTournamentId = node.SelectSingleNode("ns1:ArrangementId", _namespaceManager);
            if (nodeTournamentId != null)
            {
                tournament.TournamentId = Convert.ToInt32(nodeTournamentId.InnerText);
            }

            var nodeTournamentName = node.SelectSingleNode("ns1:ArrangementName", _namespaceManager);
            if (nodeTournamentName != null)
            {
                tournament.TournamentName = nodeTournamentName.InnerText;
            }

            return tournament;
        }

        private BetObject GetBetobjectValues(XmlNode node)
        {
            var betObject = new BetObject();
            var nodeObjectId = node.SelectSingleNode("ns1:BetObject[@Name='HUB-fulltid']", _namespaceManager);
            if (nodeObjectId != null && nodeObjectId.Attributes != null)
            {
                betObject.Id = Convert.ToInt32(nodeObjectId.Attributes["Id"].Value);
            }

            var nodeObjectNumber = node.SelectSingleNode("ns1:BetObject[@Name='HUB-fulltid']/ns1:BetObjectNumber",
                                                           _namespaceManager);

            if (nodeObjectNumber != null) betObject.Number = Convert.ToInt32(nodeObjectNumber.InnerText);

            var nodeHomeObjectOdds = node.SelectSingleNode("ns1:BetObject[@Name='HUB-fulltid']/ns1:Selection[ns1:SelectionName='H']/ns1:SelectionOdds",
                                                           _namespaceManager);
            if (nodeHomeObjectOdds != null)
            {
                betObject.HomeSelectionOdds = nodeHomeObjectOdds.InnerText;
            }

            var nodeDrawObjectOdds = node.SelectSingleNode("ns1:BetObject[@Name='HUB-fulltid']/ns1:Selection[ns1:SelectionName='U']/ns1:SelectionOdds",
                                                           _namespaceManager);
            if (nodeDrawObjectOdds != null)
            {
                betObject.DrawSelectionOdds = nodeDrawObjectOdds.InnerText;
            }

            var nodeAwayObjectOdds = node.SelectSingleNode("ns1:BetObject[@Name='HUB-fulltid']/ns1:Selection[ns1:SelectionName='B']/ns1:SelectionOdds",
                                                           _namespaceManager);
            if (nodeAwayObjectOdds != null)
            {
                betObject.AwaySelectionOdds = nodeAwayObjectOdds.InnerText;
            }

            return betObject;
        }
    }
}