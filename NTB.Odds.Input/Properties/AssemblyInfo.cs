﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using log4net.Config;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NTB.Odds.Input")]
[assembly: AssemblyDescription("Input tool to write articles to odds")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Norsk Telegrambyrå AS")]
[assembly: AssemblyProduct("NTB.Odds.Input")]
[assembly: AssemblyCopyright("Copyright ©  2014 Norsk Telegrambyrå AS")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: XmlConfigurator(ConfigFile = "web.config", Watch = true)]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("000b2517-e99f-4d00-b2e3-1a7fb8cd138e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("NTB.Odds.Input.Tests")]