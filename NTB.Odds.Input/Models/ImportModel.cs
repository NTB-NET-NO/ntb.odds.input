﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using NTB.Odds.Input.DataMappers.Matches;
using NTB.Odds.Input.DataMappers.Sports;
using NTB.Odds.Input.DataMappers.Tournaments;
using NTB.Odds.Input.Domain.Classes;
using NTB.Odds.Input.Helpers.Import;

namespace NTB.Odds.Input.Models
{
    public class ImportModel
    {
        /// <summary>
        ///     Get the content of the Langodds File to be processed
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public XmlDocument GetDocumentContent(string filename)
        {
            string path = ConfigurationManager.AppSettings["InputFolder"];

            string fullName = path + @"/" + filename;
            FileInfo fi = new FileInfo(fullName);

            if (!fi.Exists)
            {
                throw new Exception("File does not exist!");
            }

            if (fi.Length == 0)
            {
                throw new Exception("File contains no information");
            }

            var doc = new XmlDocument();
            try
            {
                doc.Load(fullName);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
                
            }

            return doc;
        }

        


        public List<ImportFile> GetFiles()
        {
            var importFiles = new List<ImportFile>();
            var inputFolder = ConfigurationManager.AppSettings["InputFolder"];

            if (string.IsNullOrEmpty(inputFolder))
            {
                throw new Exception("No Input Folder Defined!");
            }

            DirectoryInfo di = new DirectoryInfo(inputFolder);
            if (!di.Exists)
            {
                // Then we create it, but then we also know it does not contain any files
                di.Create();
            }

            

            var files = di.GetFiles().ToList();

            foreach (var file in files.Where(x => x.Length > 0))
            {

                var importFile = new ImportFile
                    {
                        FullName = file.FullName,
                        FileName = file.Name,
                        DirectoryName = file.DirectoryName,
                        CreationDate = file.CreationTime,
                        FileSize = file.Length
                    };

                importFiles.Add(importFile);
            }

            return importFiles.OrderByDescending(x => x.CreationDate).ToList();
        }


        internal void ImportContent(XmlDocument doc)
        {
            var helper = new ImportHelper(doc);
            var title = helper.GetDocumentTitle();
            var weekNumber = helper.GetWeekNumber();

            var matches = helper.GetMatches();

            var tournamentDataMapper = new TournamentDataMapper();
            var sportDataMapper = new SportDataMapper();
            var matchDataMapper = new MatchDataMapper();
            
            foreach (Match match in matches)
            {
                var sport = new Sport
                    {
                        Id = match.SportId,
                        Name = match.SportName
                    };

                var foundSport = sportDataMapper.Get(match.SportId);
                if (foundSport.Id != match.SportId)
                {
                    sportDataMapper.InsertOne(sport);
                }

                var tournament = new Tournament
                {
                    TournamentId = match.TournamentId,
                    TournamentName = match.TournamentName,
                    SportId = match.SportId
                };

                var foundTournament = tournamentDataMapper.Get(tournament.TournamentId);

                if (foundTournament.TournamentId == 0)
                {
                    tournamentDataMapper.InsertOne(tournament);
                }

                var foundMatch = matchDataMapper.Get(match.MatchId);

                if (foundMatch.MatchId == 0)
                {
                    matchDataMapper.InsertOne(match);
                }
                else
                {
                    matchDataMapper.Delete(match);
                    matchDataMapper.InsertOne(match);
                }
            }
        }

        internal void DeleteFile(string filename)
        {
            string path = ConfigurationManager.AppSettings["InputFolder"];

            string fullName = path + @"/" + filename;
            FileInfo fi = new FileInfo(fullName);

            if (!fi.Exists)
            {
                return;
            }

            fi.Delete();

        }
    }
}