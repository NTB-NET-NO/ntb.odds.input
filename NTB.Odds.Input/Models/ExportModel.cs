﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Security;
using NTB.Odds.Input.DataMappers.Matches;
using NTB.Odds.Input.DataMappers.Sports;
using NTB.Odds.Input.DataMappers.UserProfiles;
using NTB.Odds.Input.Domain.Classes;
using NTB.Odds.Input.Helpers.Export;

namespace NTB.Odds.Input.Models
{
    public class ExportModel
    {
        // we are to get data from database and the create the Xml File

        private string GetWeekNumber()
        {
            var currentCulture = CultureInfo.CurrentCulture;
            var weekNo = currentCulture.Calendar.GetWeekOfYear(
                            DateTime.Now,
                            currentCulture.DateTimeFormat.CalendarWeekRule,
                            currentCulture.DateTimeFormat.FirstDayOfWeek);

            return weekNo.ToString();
        }

        private string CreateDocumentTitle()
        {
            return "Langodds uke " + GetWeekNumber();
        }
        public void GenerateXml()
        {
            var xmlFileCreator = new XmlFileCreator();
            var dataMapper = new MatchDataMapper();
            var sportDataMapper = new SportDataMapper();
            
            List<Sport> sports = sportDataMapper.GetDocumentSports();
            xmlFileCreator.Sports = sports;
            xmlFileCreator.DocumentTitle = CreateDocumentTitle();

            var userProfileDataMapper = new UserProfileDataMapper();
            var userId = Membership.GetUser().ProviderUserKey.ToString();
            var userProfile = userProfileDataMapper.GetUserProfile(new Guid(userId));
            xmlFileCreator.UserProfile = userProfile;
            
            var matches = dataMapper.GetTodaysMatches();
            
            
            List<BettingEvent> bettingEvents = new List<BettingEvent>();
            foreach (Match match in matches)
            {
                var bettingEvent = new BettingEvent();
                bettingEvent.EventId = match.MatchId;

                var homeTeam = new Team {TeamId = match.HomeTeamId, TeamName = match.HomeTeam};
                var awayTeam = new Team {TeamId = match.AwayTeamId,TeamName = match.AwayTeam };
                bettingEvent.HomeTeam = homeTeam;
                bettingEvent.AwayTeam = awayTeam;

                var tournament = new Tournament
                    {
                        TournamentId = match.TournamentId,
                        TournamentName = match.TournamentName
                    };
                bettingEvent.Tournament = tournament;
                
                bettingEvent.Description = match.MatchComment;
                bettingEvent.MatchStartDate = match.MatchDate;
                bettingEvent.MatchStartTime = match.MatchStartTime;
                bettingEvent.Broadcaster = match.Broadcaster;
                bettingEvent.SelectedMatchType = match.SelectedMatchType;
                bettingEvent.SelectedOdds = match.SelectedOdds;
                
                
                List<BettingSelection> selections = new List<BettingSelection>();
                var selection = new BettingSelection
                    {
                        Id = 1,
                        Name = "home",
                        ShortName = "H",
                        Value = match.BetObject.HomeSelectionOdds,
                        WithDrawn = false
                    };

                selections.Add(selection);
                
                selection = new BettingSelection
                {
                    Id = 2,
                    Name = "draw",
                    ShortName = "U",
                    Value = match.BetObject.DrawSelectionOdds,
                    WithDrawn = false
                };

                selections.Add(selection);
                selection = new BettingSelection
                {
                    Id = 3,
                    Name = "away",
                    ShortName = "B",
                    Value = match.BetObject.AwaySelectionOdds,
                    WithDrawn = false
                };
                selections.Add(selection);

                var bettingObject = new BettingObject
                    {
                        Id = match.MatchId,
                        Name = "HUB-fulltid",
                        Type = "odds",
                        Selections = selections
                    };



                bettingEvent.BettingObject = bettingObject;

                bettingEvents.Add(bettingEvent);
            }
            // Adding betting events to object
            xmlFileCreator.BettingEvents = bettingEvents;

            // Create XML
            xmlFileCreator.CreateXml();
        }
    }
}