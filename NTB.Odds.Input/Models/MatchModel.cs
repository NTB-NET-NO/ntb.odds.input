﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using NTB.Odds.Input.DataMappers.Matches;
using NTB.Odds.Input.Domain.Classes;
using NTB.Odds.Input.Interfaces.DataMappers;

namespace NTB.Odds.Input.Models
{
    /// <summary>
    ///     This class is used to create the output for showing matches in the view
    /// </summary>
    public class MatchModel
    {
        
        public List<Match> GetTodaysMatches()
        {
            var matchDataMapper = new MatchDataMapper();
            return matchDataMapper.GetTodaysMatches();
        }

        internal void StoreMatchComment(FormCollection collection)
        {
            int matchId = Convert.ToInt32(collection["matchid"]);
            string matchComment = collection["vurdering_" + matchId];
            string matchOdds = collection["matchodds_" + matchId];
            string matchType = collection["matchtype_" + matchId];
            string broadcaster = collection["tv_" + matchId];

            int broadcasterId = 0;
            if (broadcaster == string.Empty)
            {
                if (collection["sel_tv_" + matchId] != string.Empty)
                {
                    broadcasterId = Convert.ToInt32(collection["sel_tv_" + matchId]);
                }
            }

            IMatchDataMapper dataMapper = new MatchDataMapper();
            dataMapper.StoreMatchComment(matchId, matchComment, matchOdds, matchType, broadcaster, broadcasterId);

        }

        internal List<Match> GetMatchesByDate(DateTime matchDate)
        {
            var matchDataMapper = new MatchDataMapper();
            return matchDataMapper.GetMatchesByDate(matchDate);
        }
    }
}