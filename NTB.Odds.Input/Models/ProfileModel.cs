﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using NTB.Odds.Input.DataMappers.UserProfiles;
using NTB.Odds.Input.Domain.Classes;

namespace NTB.Odds.Input.Models
{
    public class ProfileModel
    {
        public UserProfile GetProfile()
        {
            UserProfile userProfile = new UserProfile();
            var membershipUser = Membership.GetUser();

            if (membershipUser != null)
            {
                if (membershipUser.ProviderUserKey != null)
                {
                    var user = membershipUser.ProviderUserKey.ToString();
                    var userId = new Guid(user);

                    var profileDataMapper = new UserProfileDataMapper();
                    userProfile = profileDataMapper.GetUserProfile(userId);
                }
            }

            return userProfile;
        }
    }
}