﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.Odds.Input.Domain.Classes;
using NTB.Odds.Input.Interfaces.Common;
using NTB.Odds.Input.Interfaces.DataMappers;
using log4net;

namespace NTB.Odds.Input.DataMappers.Documents
{
    public class DocumentDataMapper : IRepository<Document>, IDisposable, IDocumentDataMapper
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(DocumentDataMapper));

        public int InsertOne(Document domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_InsertDocument", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentName", domainobject.Name));
                    sqlCommand.Parameters.Add(new SqlParameter("@CreationDate", domainobject.CreationDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@CreationTime", domainobject.CreationTime));
                    sqlCommand.Parameters.Add(new SqlParameter("@DocumentVersion", domainobject.Version));
                    
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return 1;
            }
        }

        public void InsertAll(List<Document> domainobject)
        {
            foreach (Document document in domainobject)
            {
                InsertOne(document);
            }
        }

        public void Update(Document domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Document domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Document> GetAll()
        {
            throw new NotImplementedException();
        }

        public Document Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void StoreDocument()
        {
            throw new NotImplementedException();
        }

        public List<Document> GetDocumentsByDate(DateTime creationDate)
        {
            var documents = new List<Document>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_GetDocumentsByDate", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.Add(new SqlParameter("@CreationDate", creationDate));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }


                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return documents;
                    }

                    while (sqlDataReader.Read())
                    {
                        var document = new Document();

                        document.Name = sqlDataReader["DocumentName"].ToString();
                        document.CreationDate = Convert.ToDateTime(sqlDataReader["CreationDate"].ToString());
                        document.CreationTime = Convert.ToInt32(sqlDataReader["CreationTime"]);
                        document.Version = Convert.ToInt32(sqlDataReader["DocumentVersion"]);


                        documents.Add(document);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return documents.ToList();
            }
        }
    }
}