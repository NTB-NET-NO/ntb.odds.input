﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.Odds.Input.Domain.Classes;
using NTB.Odds.Input.Interfaces.Common;
using NTB.Odds.Input.Interfaces.DataMappers;
using log4net;

namespace NTB.Odds.Input.DataMappers.Tournaments
{
    public class TournamentDataMapper : IRepository<Tournament>, IDisposable, ITournamentDataMapper
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(TournamentDataMapper));

        public int InsertOne(Tournament domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_InsertTournament", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentName", domainobject.TournamentName));
                    
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return 1;
            }
        }

        public void InsertAll(List<Tournament> domainobject)
        {
            foreach (Tournament tournament in domainobject)
            {
                InsertOne(tournament);
            }
        }

        public void Update(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Tournament domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Tournament> GetAll()
        {
            throw new NotImplementedException();
        }

        public Tournament Get(int id)
        {
            var tournament = new Tournament();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_GetTournamentById", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", id));
                    
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return tournament;
                    }

                    while (sqlDataReader.Read())
                    {
                        tournament.TournamentId = Convert.ToInt32(sqlDataReader["TournamentId"]);
                        tournament.SportId = Convert.ToInt32(sqlDataReader["SportId"]);
                        tournament.TournamentName = sqlDataReader["TournamentId"].ToString();
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return tournament;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Tournament> GetTodaysMatches()
        {
            return new List<Tournament>();
        }

        public void InsertTournamentFromMatches(List<Match> matches)
        {
            
        }
    }
}