﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.Odds.Input.Domain.Classes;
using NTB.Odds.Input.Interfaces.Common;
using NTB.Odds.Input.Interfaces.DataMappers;
using log4net;

namespace NTB.Odds.Input.DataMappers.Matches
{
    public class MatchDataMapper : IRepository<Match>, IDisposable, IMatchDataMapper
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(MatchDataMapper));

        public int InsertOne(Match domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_InsertMatch", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", domainobject.MatchId));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.SportId));
                    sqlCommand.Parameters.Add(new SqlParameter("@TournamentId", domainobject.TournamentId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamId", domainobject.AwayTeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@AwayTeamName", domainobject.AwayTeam));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamId", domainobject.HomeTeamId));
                    sqlCommand.Parameters.Add(new SqlParameter("@HomeTeamName", domainobject.HomeTeam));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", domainobject.MatchDate));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchStartTime", domainobject.MatchStartTime));
                    // 
                    sqlCommand.Parameters.Add(new SqlParameter("@HomeOdds", domainobject.BetObject.HomeSelectionOdds));
                    sqlCommand.Parameters.Add(new SqlParameter("@DrawOdds", domainobject.BetObject.DrawSelectionOdds));
                    sqlCommand.Parameters.Add(new SqlParameter("@AwayOdds", domainobject.BetObject.AwaySelectionOdds));
                    

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return 1;
            }
        }

        public void InsertAll(List<Match> domainobject)
        {
            foreach (Match match in domainobject)
            {
                InsertOne(match);
            }
        }

        public void Update(Match domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Match domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_DeleteMatchById", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", domainobject.MatchId));

                    sqlCommand.ExecuteNonQuery();

                    
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

            }
        }

        public IQueryable<Match> GetAll()
        {
            throw new NotImplementedException();
        }

        public Match Get(int id)
        {
            var match = new Match();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_GetMatchById", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", id));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return match;
                    }

                    if (sqlDataReader.Read())
                    {
                        match.MatchId = (int) sqlDataReader["MatchId"];
                        match.SportId = (int) sqlDataReader["SportId"];
                        match.SportName = sqlDataReader["SportName"].ToString();
                        match.TournamentId = (int) sqlDataReader["TournamentId"];
                        match.TournamentName = sqlDataReader["TournamentName"].ToString();
                        match.HomeTeamId = Convert.ToInt32(sqlDataReader["HomeTeamId"]);
                        match.HomeTeam = sqlDataReader["HomeTeamName"].ToString();
                        match.AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.AwayTeam = sqlDataReader["AwayTeamName"].ToString();
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"].ToString());
                        match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchTime"]);
                        match.MatchComment = sqlDataReader["Article"].ToString();
                        match.SelectedOdds = sqlDataReader["SelectedOdds"].ToString();
                        match.SelectedMatchType = sqlDataReader["SelectedMatchType"].ToString();

                        BetObject betObject = new BetObject();
                        betObject.HomeSelectionOdds = sqlDataReader["HomeOdds"].ToString();
                        betObject.DrawSelectionOdds = sqlDataReader["DrawOdds"].ToString();
                        betObject.AwaySelectionOdds = sqlDataReader["AwayOdds"].ToString();

                        match.BetObject = betObject;
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return match;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Match> GetMatchesByDate(DateTime matchDate)
        {
            var matches = new List<Match>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_GetMatchesByDate", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", matchDate));

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", DateTime.Today.Date));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return matches;
                    }

                    while (sqlDataReader.Read())
                    {
                        var match = new Match();
                        var broadcaster = new Broadcaster();

                        match.MatchId = (int) sqlDataReader["MatchId"];
                        match.SportId = (int) sqlDataReader["SportId"];
                        match.SportName = sqlDataReader["SportName"].ToString();
                        match.TournamentId = (int) sqlDataReader["TournamentId"];
                        match.TournamentName = sqlDataReader["TournamentName"].ToString();
                        match.HomeTeamId = Convert.ToInt32(sqlDataReader["HomeTeamId"]);
                        match.HomeTeam = sqlDataReader["HomeTeamName"].ToString();
                        match.AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.AwayTeam = sqlDataReader["AwayTeamName"].ToString();
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"].ToString());
                        match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchTime"]);
                        match.MatchComment = sqlDataReader["Article"].ToString();
                        match.SelectedOdds = sqlDataReader["SelectedOdds"].ToString();
                        match.SelectedMatchType = sqlDataReader["SelectedMatchType"].ToString();
                        broadcaster.Name = sqlDataReader["BroadcasterName"].ToString();

                        if (sqlDataReader["BroadcasterId"] != DBNull.Value)
                        {
                            broadcaster.Id = Convert.ToInt32(sqlDataReader["BroadcasterId"]);
                        }
                        else
                        {
                            broadcaster.Id = 0;
                        }
                        match.Broadcaster = broadcaster;

                        BetObject betObject = new BetObject();
                        betObject.HomeSelectionOdds = sqlDataReader["HomeOdds"].ToString();
                        betObject.DrawSelectionOdds = sqlDataReader["DrawOdds"].ToString();
                        betObject.AwaySelectionOdds = sqlDataReader["AwayOdds"].ToString();

                        match.BetObject = betObject;

                        matches.Add(match);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches.ToList();
            }
        }


        public List<Match> GetTodaysMatches()
        {
            var matches = new List<Match>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_GetTodaysMatches", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    // sqlCommand.Parameters.Add(new SqlParameter("@MatchDate", DateTime.Today.Date));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return matches;
                    }
                    
                    while (sqlDataReader.Read())
                    {
                        var match = new Match();
                        var broadcaster = new Broadcaster();
                        
                        match.MatchId = (int) sqlDataReader["MatchId"];
                        match.SportId = (int) sqlDataReader["SportId"];
                        match.SportName = sqlDataReader["SportName"].ToString();
                        match.TournamentId = (int) sqlDataReader["TournamentId"];
                        match.TournamentName = sqlDataReader["TournamentName"].ToString();
                        match.HomeTeamId = Convert.ToInt32(sqlDataReader["HomeTeamId"]);
                        match.HomeTeam = sqlDataReader["HomeTeamName"].ToString();
                        match.AwayTeamId = Convert.ToInt32(sqlDataReader["AwayTeamId"]);
                        match.AwayTeam = sqlDataReader["AwayTeamName"].ToString();
                        match.MatchDate = Convert.ToDateTime(sqlDataReader["MatchDate"].ToString());
                        match.MatchStartTime = Convert.ToInt32(sqlDataReader["MatchTime"]);
                        match.MatchComment = sqlDataReader["Article"].ToString();
                        match.SelectedOdds = sqlDataReader["SelectedOdds"].ToString();
                        match.SelectedMatchType = sqlDataReader["SelectedMatchType"].ToString();
                        broadcaster.Name = sqlDataReader["BroadcasterName"].ToString();

                        if (sqlDataReader["BroadcasterId"] != DBNull.Value)
                        {
                            broadcaster.Id = Convert.ToInt32(sqlDataReader["BroadcasterId"]);
                        }
                        else
                        {
                            broadcaster.Id = 0;
                        }
                        match.Broadcaster = broadcaster;

                        BetObject betObject = new BetObject();
                        betObject.HomeSelectionOdds = sqlDataReader["HomeOdds"].ToString();
                        betObject.DrawSelectionOdds = sqlDataReader["DrawOdds"].ToString();
                        betObject.AwaySelectionOdds = sqlDataReader["AwayOdds"].ToString();

                        match.BetObject = betObject;

                        matches.Add(match);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return matches.ToList();
            }
        }

        
        public void StoreMatchComment(int matchId, string matchComment, string matchOdds, string matchType, string broadcaster, int broadcasterId)
        {
            Logger.Info("Storing article");
            Logger.Debug("Broadcaster: " + broadcaster);
            Logger.Debug("Broadcaster-id: " + broadcasterId);
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_StoreMatchComment", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchComment", matchComment));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchOdds", matchOdds));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchType", matchType));
                    if (broadcaster.Trim() != string.Empty)
                    {
                        sqlCommand.Parameters.Add(new SqlParameter("@Broadcaster", broadcaster));
                    }

                    if (broadcasterId > 0)
                    {
                        sqlCommand.Parameters.Add(new SqlParameter("@BroadcasterId", broadcasterId));
                    }
                    
                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}