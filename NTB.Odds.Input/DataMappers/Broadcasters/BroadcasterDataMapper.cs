﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.Odds.Input.Domain.Classes;
using NTB.Odds.Input.Interfaces.Common;
using NTB.Odds.Input.Interfaces.DataMappers;
using log4net;
namespace NTB.Odds.Input.DataMappers.Broadcasters
{
    public class BroadcasterDataMapper : IRepository<Broadcaster>, IDisposable, IBroadcasterDataMapper 
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(BroadcasterDataMapper));

        public int InsertOne(Broadcaster domainobject)
        {
            throw new NotImplementedException();
        }

        public void InsertAll(List<Broadcaster> domainobject)
        {
            throw new NotImplementedException();
        }

        public void Update(Broadcaster domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_UpdateBroadcaster", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@BroadcasterId", domainobject.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@Broadcaster", domainobject.Name));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }

        public void Delete(Broadcaster domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Broadcaster> GetAll()
        {
            List<Broadcaster> broadcasters = new List<Broadcaster>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_GetAllBroadcasters", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    if (!sqlDataReader.HasRows)
                    {
                        return broadcasters.AsQueryable();
                    }

                    while (sqlDataReader.Read())
                    {
                        Broadcaster broadcaster = new Broadcaster
                            {
                                Id = Convert.ToInt32(sqlDataReader["Id"]),
                                Name = sqlDataReader["Name"].ToString()
                            };
                    broadcasters.Add(broadcaster);
                    }
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            return broadcasters.AsQueryable();
                
            }
        }

        public Broadcaster Get(int id)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
        
        public void InsertBroadcaster(string broadcaster, int matchId)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_InsertBroadcaster", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    sqlCommand.Parameters.Add(new SqlParameter("@Broadcaster", broadcaster));
                    sqlCommand.Parameters.Add(new SqlParameter("@MatchId", matchId));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);

                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }
            }
        }
    }
}