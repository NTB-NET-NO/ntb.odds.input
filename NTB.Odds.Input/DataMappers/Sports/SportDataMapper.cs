﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using NTB.Odds.Input.Domain.Classes;
using NTB.Odds.Input.Interfaces.Common;
using NTB.Odds.Input.Interfaces.DataMappers;
using log4net;

namespace NTB.Odds.Input.DataMappers.Sports
{
    public class SportDataMapper : IRepository<Sport>, IDisposable, ISportDataMapper 
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(SportDataMapper));

        public int InsertOne(Sport domainobject)
        {
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_InsertSport", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    
                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", domainobject.Id));
                    sqlCommand.Parameters.Add(new SqlParameter("@SportName", domainobject.Name));

                    sqlCommand.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return 1;
            }
        }

        public void InsertAll(List<Sport> domainobject)
        {
            foreach (Sport sport in domainobject)
            {
                InsertOne(sport);
            }
        }

        public void Update(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        public void Delete(Sport domainobject)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Sport> GetAll()
        {
            throw new NotImplementedException();
        }

        public Sport Get(int id)
        {
            var sport = new Sport();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_GetSportById", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }

                    sqlCommand.Parameters.Add(new SqlParameter("@SportId", id));

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return sport;
                    }

                    while (sqlDataReader.Read())
                    {
                        sport.Id = Convert.ToInt32(sqlDataReader["SportId"]);
                        sport.Name = sqlDataReader["SportName"].ToString();
                        sport.IptcCode = Convert.ToInt32(sqlDataReader["IptcCode"]);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return sport;
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<Sport> GetDocumentSports()
        {
            var sports = new List<Sport>();
            using (
                var sqlConnection =
                    new SqlConnection(
                        ConfigurationManager.ConnectionStrings["TippeDataConnectionString"].ConnectionString))
            {
                try
                {
                    var sqlCommand = new SqlCommand("TippeData_GetDocumentSports", sqlConnection);

                    sqlCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlConnection.State == ConnectionState.Closed)
                    {
                        sqlConnection.Open();
                    }
                    
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    if (!sqlDataReader.HasRows)
                    {
                        return sports;
                    }

                    while (sqlDataReader.Read())
                    {
                        var sport = new Sport
                            {
                                Id = Convert.ToInt32(sqlDataReader["SportId"]),
                                Name = sqlDataReader["SportName"].ToString(),
                                IptcCode = Convert.ToInt32(sqlDataReader["IptcCode"])
                            };

                        sports.Add(sport);
                    }

                }
                catch (Exception exception)
                {
                    Logger.Error(exception.Message);
                    Logger.Error(exception.StackTrace);
                }
                finally
                {
                    if (sqlConnection.State == ConnectionState.Open)
                    {
                        sqlConnection.Close();
                    }
                }

                return sports;
            }
        }
    }
}