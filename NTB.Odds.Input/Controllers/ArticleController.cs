﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NTB.Odds.Input.DataMappers.Broadcasters;
using NTB.Odds.Input.DataMappers.Documents;
using NTB.Odds.Input.Domain.Classes;
using NTB.Odds.Input.Models;

namespace NTB.Odds.Input.Controllers
{
    [Authorize(Roles = "Administrator,Registrator")]
    public class ArticleController : Controller
    {
        //
        // GET: /Article/

        public ActionResult Index()
        {
            var matchModel = new MatchModel();
            var broadcasterDataMapper = new BroadcasterDataMapper();
            var modelView = new ArticleViewModel();
            modelView.Matches = matchModel.GetTodaysMatches();
            modelView.Broadcasters = broadcasterDataMapper.GetAll().ToList();
            return View(modelView);
        }

        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {
            var matchModel = new MatchModel();
            var broadcasterDataMapper = new BroadcasterDataMapper();
            var modelView = new ArticleViewModel();

            // Create the date
            if (collection["matchDate"] != string.Empty)
            {
                var selectedMatchDate = DateTime.Parse(collection["matchDate"]);

                modelView.Matches = matchModel.GetMatchesByDate(selectedMatchDate);
                modelView.Broadcasters = broadcasterDataMapper.GetAll().ToList();
                return View(modelView);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult CreateXml(FormCollection collection)
        {

            // We are to create and store the Xml-file
            var model = new ExportModel();
            model.GenerateXml();
            return RedirectToAction("ListDocuments", "Article");
        }
        //
        // GET: /Article/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Article/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Article/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        //
        // GET: /Article/Edit/5
 
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Article/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Article/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Article/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult PostComment(int id, FormCollection collection)
        {
            MatchModel model = new MatchModel();
            model.StoreMatchComment(collection);
            
            return null;
        }

        public ActionResult ListDocuments()
        {
            var dataMapper = new DocumentDataMapper();
            return View(dataMapper.GetDocumentsByDate(DateTime.Today));
        }
    }
}
