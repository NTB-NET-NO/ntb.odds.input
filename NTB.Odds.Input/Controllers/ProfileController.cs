﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using NTB.Odds.Input.DataMappers.UserProfiles;
using NTB.Odds.Input.Domain.Classes;

namespace NTB.Odds.Input.Controllers
{
    public class ProfileController : Controller
    {
        //
        // GET: /Profile/

        public ActionResult Index()
        {
            var userProfileDataMapper = new UserProfileDataMapper();
            var user = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            var userProfile = userProfileDataMapper.GetUserProfile(user);
            return View(userProfile);
            
        }

        //
        // GET: /Profile/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Profile/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Profile/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        
        public ActionResult Edit(string id)
        {
            var userProfileDataMapper = new UserProfileDataMapper();
            var user = new Guid(id);
            var userProfile = userProfileDataMapper.GetUserProfile(user);
            return View(userProfile);
        }
        
        //
        // GET: /Profile/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //
        // POST: /Profile/Edit/5

        [HttpPost]
        public ActionResult Edit(string id, FormCollection collection)
        {
            try
            {
                // Get the UserId from the URL
                var foo = new Guid(id);
                var user = new Guid(Membership.GetUser().ProviderUserKey.ToString());                
                var userProfile = new UserProfile
                    {
                        FirstName = collection["firstname"],
                        LastName = collection["lastname"],
                        Signature = collection["signature"],
                        UserId = user
                    };

                var userProfileDataMapper = new UserProfileDataMapper();

                var u = userProfileDataMapper.GetUserProfile(user);
                if (u.FirstName == null)
                {
                    userProfileDataMapper.InsertProfile(userProfile);
                }
                else
                {
                    userProfileDataMapper.Update(userProfile);
                }
                

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Profile/Delete/5
 
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Profile/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
 
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
