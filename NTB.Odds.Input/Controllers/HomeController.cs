﻿using System.Web.Mvc;
using log4net;

namespace NTB.Odds.Input.Controllers
{
    public class HomeController : Controller
    {
        internal static readonly ILog Logger = LogManager.GetLogger(typeof(HomeController));

        public ActionResult Index()
        {
            ViewBag.Message = "NTB Odds Input!";
            Logger.Info("Writing to log");
            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
