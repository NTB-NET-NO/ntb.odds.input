﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NTB.Odds.Input.Interfaces.DataMappers
{
    interface IBroadcasterDataMapper
    {
        void InsertBroadcaster(string broadcaster, int matchId);
    }
}
