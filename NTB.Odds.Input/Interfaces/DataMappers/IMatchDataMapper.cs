﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NTB.Odds.Input.Domain.Classes;

namespace NTB.Odds.Input.Interfaces.DataMappers
{
    interface IMatchDataMapper
    {
        List<Match> GetTodaysMatches();
        void StoreMatchComment(int matchId, string matchComment, string matchOdds, string matchType, string broadcaster, int broadcasterId);
    }
}
