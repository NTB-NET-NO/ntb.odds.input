﻿using System;
using System.Linq;
using NTB.Odds.Input.Domain.Classes;

namespace NTB.Odds.Input.Interfaces.DataMappers
{
    interface IUserProfileDataMapper
    {
        IQueryable<UserProfile> GetUserProfiles();
        UserProfile GetUserProfile(Guid userId);
    }
}
