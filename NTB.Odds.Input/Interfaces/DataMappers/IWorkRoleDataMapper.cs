﻿using System;
using NTB.Odds.Input.Domain.Classes;

namespace NTB.Odds.Input.Interfaces.DataMappers
{
    interface IWorkRoleDataMapper
    {
        void DeleteAll();
        WorkRole GetWorkRoleByUserId(Guid userId);
    }
}
